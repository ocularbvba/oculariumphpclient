<?php

namespace Ocular\Ocularium;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Ocular\Ocularium\Exception\HostNotFound;
use Ocular\Ocularium\Exception\ProjectNotFound;
use Ocular\Ocularium\Exception\ScenarioNotFound;
use Ocular\Ocularium\Exception\ServerError;
use Ocular\Ocularium\Helper\HTTP;

class Client
{

    /**
     * @var array
     */
    private $header;

    /**
     * @var string
     */
    private $host;


    /**
     * Client constructor.
     * @param  string  $host
     * @param  string  $key
     */
    public function __construct(string $host, string $key)
    {
        $this->host = $host;
        $this->header = ['X-API-KEY' => $key];
    }

    /**
     * @return array
     * @throws ProjectNotFound
     * @throws ServerError
     * @throws HostNotFound
     */
    public function getAllScenariosOfProject(): array
    {
        try {
            $response = HTTP::get($this->host, '/external/v1/scenarios', $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() === 404) {
                throw new ProjectNotFound('Project Not Found');
            }

            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @param  int  $scenarioId
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     * @throws ScenarioNotFound
     */
    public function activateScenario(int $scenarioId): array
    {
        try {
            $response = HTTP::put($this->host, '/external/v1/scenarios/'.$scenarioId.'/activate', [], $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() === 404) {
                throw new ScenarioNotFound('Scenario Not Found');
            }

            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function nextKeypoint(): array
    {
        try {
            $response = HTTP::get($this->host, '/external/v1/control/next', $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function playKeypoint(): array
    {
        try {
            $response = HTTP::get($this->host, '/external/v1/control/play', $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function previousKeypoint(): array
    {
        try {
            $response = HTTP::get($this->host, '/external/v1/control/previous', $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @param  string  $name
     * @param  bool  $hasIntroduction
     * @param  string  $moduleType
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function createModule(string $name, bool $hasIntroduction, string $moduleType): array
    {
        try {
            $response = HTTP::post($this->host, '/external/v1/modules', [
                'name' => $name,
                'hasIntroduction' => $hasIntroduction,
                'moduleType' => $moduleType
            ], $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @param  int  $moduleId
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function getModule(int $moduleId): array
    {
        try {
            $response = HTTP::get($this->host, '/external/v1/modules/'.$moduleId, $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @param  int  $moduleId
     * @param  string  $name
     * @param  bool  $hasIntroduction
     * @param  array  $data
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function updateModule(int $moduleId, string $name, bool $hasIntroduction, array $data): array
    {
        try {
            $response = HTTP::put($this->host, '/external/v1/modules/'.$moduleId, [
                'name' => $name,
                'hasIntroduction' => $hasIntroduction,
                'data' => $data
            ], $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @param  int  $moduleId
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function deleteModule(int $moduleId): array
    {
        try {
            $response = HTTP::delete($this->host, '/external/v1/modules/'.$moduleId, $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @param  int  $moduleId
     * @param  array  $assets
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function addAssetsToModules(int $moduleId, array $assets = []): array
    {
        try {
            $response = HTTP::post($this->host, '/external/v1/modules/'.$moduleId.'/assets', $assets, $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }

    /**
     * @param  int  $deviceId
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     */
    public function previewDevice(int $deviceId): array
    {
        try {
            $response = HTTP::get($this->host, '/external/v1/devices/'.$deviceId.'/preview', $this->header);

            if ($response === null) {
                throw new ServerError('Server has returned an error');
            }

            return json_decode($response->getBody(), true);
        } catch (ClientException $e) {
            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [];
    }
}
