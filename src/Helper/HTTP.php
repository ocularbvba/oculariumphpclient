<?php

namespace Ocular\Ocularium\Helper;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

abstract class HTTP
{
    /**
     * @param  string  $baseUri
     * @param  string  $path
     * @param  array  $headers
     * @return ResponseInterface|null
     * @throws RequestException
     */
    public static function get(
        string $baseUri,
        string $path,
        array $headers = []
    ): ?ResponseInterface {
        try {
            $client = new Client(['base_uri' => $baseUri, 'verify' => false]);
            $request = new Request('GET', $path, $headers);
            return $client->send($request);
        } catch (RequestException $e) {
            throw $e;
        }
    }

    /**
     * @param  string  $baseUri
     * @param  string  $path
     * @param  array|string  $body
     * @param  array  $headers
     * @return ResponseInterface
     * @throws RequestException
     */
    public static function put(
        string $baseUri,
        string $path,
        $body,
        array $headers = []
    ): ?ResponseInterface {
        try {
            $client = new Client(['base_uri' => $baseUri, 'headers' => $headers, 'verify' => false]);
            return $client->put($path, ['json' => $body]);
        } catch (RequestException $e) {
            throw $e;
        }
    }

    /**
     * @param  string  $baseUri
     * @param  string  $path
     * @param  array  $body
     * @param  array  $headers
     * @return ResponseInterface
     * @throws RequestException
     */
    public static function post(
        string $baseUri,
        string $path,
        array $body,
        array $headers = []
    ): ?ResponseInterface {
        try {
            $client = new Client(['base_uri' => $baseUri, 'headers' => $headers, 'verify' => false]);
            return $client->post($path, ['json' => $body]);
        } catch (RequestException $e) {
            throw $e;
        }
    }

    /**
     * @param  string  $baseUri
     * @param  string  $path
     * @param  array  $headers
     * @return ResponseInterface|null
     * @throws RequestException
     */
    public static function delete(
        string $baseUri,
        string $path,
        array $headers = []
    ): ?ResponseInterface {
        try {
            $client = new Client(['base_uri' => $baseUri, 'verify' => false]);
            $request = new Request('DELETE', $path, $headers);
            return $client->send($request);
        } catch (RequestException $e) {
            throw $e;
        }
    }
}
