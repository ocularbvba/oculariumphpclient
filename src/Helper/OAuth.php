<?php

namespace Ocular\Ocularium\Helper;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Ocular\Ocularium\Exception\HostNotFound;
use Ocular\Ocularium\Exception\InvalidClient;
use Ocular\Ocularium\Exception\ServerError;

class OAuth
{
    /**
     * @param  string  $host
     * @param  int  $clientId
     * @param  string  $clientSecret
     * @return array
     * @throws HostNotFound
     * @throws ServerError
     * @throws InvalidClient
     */
    public static function getAccessToken(string $host, int $clientId, string $clientSecret): array
    {
        $data = [
            'form_params' => [
                'grant_type' => 'client_credentials',
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'scope' => '*',

            ]
        ];

        try {
            $tokenRequest = HTTP::post(
                $host,
                'oauth/token',
                $data
            );

            if ($tokenRequest === null) {
                throw new ServerError('Server has returned an error');
            }

            $tokenRequest = json_decode($tokenRequest->getBody(), true);

            return [
                'Authorization' => $tokenRequest['token_type'].' '.$tokenRequest['access_token'],
                'Content-Type' => 'application/json',
                'Content-Accept' => 'application/json'
            ];
        } catch (ClientException $e) {
            if ($e->getCode() === 401) {
                throw new InvalidClient('Client details given are incorrect');
            }

            if ($e->getCode() === 404) {
                throw new HostNotFound('Host is not found');
            }

            if ($e->getCode() >= 500) {
                throw new ServerError('Server has returned an error');
            }
        } catch (ConnectException $e) {
            throw new HostNotFound('Host is not found');
        }

        return [
            'Authorization' => '',
            'Content-Type' => 'application/json',
            'Content-Accept' => 'application/json'
        ];
    }
}
