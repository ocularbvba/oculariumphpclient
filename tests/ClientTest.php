<?php

use Ocular\Ocularium\Client;
use PHPUnit\Framework\TestCase;

final class ClientTest extends TestCase
{

    /**
     * @var Client
     */
    private $client;

    protected function setUp(): void
    {
        $this->client = new Client(
            getenv('OCULARIUM_SERVER'),
            getenv('OCULARIUM_API_KEY'));
    }

    public function testCanClientBeConstructed()
    {
        $client = new Client(
            getenv('OCULARIUM_SERVER'),
            getenv('OCULARIUM_API_KEY'));

        $this->assertInstanceOf(Client::class, $client);
    }

    public function testCanClientGetAllScenariosOfProject()
    {
        $returnData = $this->client->getAllScenariosOfProject();

        $this->assertIsArray($returnData);
    }

    public function testCanClientActivateScenario()
    {
        $returnData = $this->client->activateScenario(1);

        $this->assertIsArray($returnData);
    }
}
